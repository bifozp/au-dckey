--[[
  ディファレンスキー 共通処理モジュール
  (c) 2018 Bifozp
]]

local m = {}

m.exec0 = function(diffproc, cache_str, p1,p2,p3)
  if "" == cache_str then
    cache_str = "diff_default"
  end

  local sc = "cache:"..cache_str

  if 0 == obj.frame then
    if not obj.copybuffer(sc, "obj") then
      debug_print("err: init failed.")
      return
    end
  end

  -- オブジェクト退避
  obj.copybuffer("tmp", "obj")

  -- キャッシュバッファ読み出し
  if not obj.copybuffer("obj", sc) then
    debug_print("err: cache read failed.")
    return
  end
  local keydt, keyw, keyh = obj.getpixeldata("alloc")

  -- 現フレーム読み出し
  obj.copybuffer("obj", "tmp")
  local wkdt, wkw, wkh = obj.getpixeldata()

  -- オブジェクトサイズの一致チェック
  -- 不一致なら処理しない
  if(wkw ~= keyw or wkh ~= keyh) then
    debug_print("Err: diffshare.exec0(): Object size mismatch.")
    return
  end

  -- キー処理
  diffproc(wkdt, keydt, wkw, wkh, p1, p2, p3)
  obj.putpixeldata(wkdt)
end

return m
